package fr.irit.tools.utils;

import java.io.File;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import fr.irit.tools.utils.ontologies.Ontology;

public class ModelLoader {
	public static Model getLoadedModel(Ontology... ontologies){
		Model m = ModelFactory.createDefaultModel();
		for(Ontology o : ontologies){
			m.read(o.getURI());
			m.setNsPrefix(o.getPrefix(), o.getURI());
		}
		return m;
	}
	
	public static Model getLoadedModel(String... uris){
		Model m = ModelFactory.createDefaultModel();
		for(String uri : uris){
			m.read(uri);
		}
		return m;
	}
	
	public static Model getLoadedModel(File... files){
		Model m = ModelFactory.createDefaultModel();
		for(File file : files){
			m.read(file.getPath());
		}
		return m;
	}
	
	public static void setModelNS(Model m, Ontology... ontologies){
		for(Ontology o : ontologies){
			m.setNsPrefix(o.getPrefix(), o.getURI());
		}
	}
}
