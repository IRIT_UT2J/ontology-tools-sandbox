package fr.irit.tools.utils;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.irit.tools.utils.ontologies.DUL;
import fr.irit.tools.utils.ontologies.IoTO;
import fr.irit.tools.utils.ontologies.Ontology;
import fr.irit.tools.utils.ontologies.SAN;
import fr.irit.tools.utils.ontologies.SSN;

import java.util.Map.Entry;

public class Prefixes {
	// Separating prefixes into several lists lightens requests
		public static Set<Entry<String, String>> IOT_PREFIXES;
		public static Set<Entry<String, String>> SERVICES_PREFIXES;
		public static Set<Entry<String, String>> ALL_PREFIXES;
		public static Map<String, String> FEATURES;
		
		static
		{
			Set<Entry<String, String>> tmp1 = new HashSet<Map.Entry<String,String>>();
			tmp1.add(new AbstractMap.SimpleEntry<String, String>( IoTO.PREFIX,"<"+IoTO.URI+">"));
			tmp1.add(new AbstractMap.SimpleEntry<String, String>(SSN.PREFIX,"<"+SSN.URI+">"));
			tmp1.add(new AbstractMap.SimpleEntry<String, String>(DUL.PREFIX, "<"+DUL.URI+">"));
			tmp1.add(new AbstractMap.SimpleEntry<String, String>(SAN.PREFIX, "<"+SAN.URI+">"));
			tmp1.add(new AbstractMap.SimpleEntry<String, String>("lifecycle",
					"<http://purl.org/vocab/lifecycle/schema#>"));
			IOT_PREFIXES = Collections.unmodifiableSet(tmp1);
			
			Set<Entry<String, String>> tmp2 = new HashSet<Map.Entry<String,String>>();
			tmp2.add(new AbstractMap.SimpleEntry<String, String>("wsmo-lite",
					"<http://www.wsmo.org/ns/wsmo-lite#>"));
			tmp2.add(new AbstractMap.SimpleEntry<String, String>("msm",
					"<http://iserve.kmi.open.ac.uk/ns/msm#>"));
			tmp2.add(new AbstractMap.SimpleEntry<String, String>("hrests",
					"<http://www.wsmo.org/ns/hrests#>"));
			SERVICES_PREFIXES = Collections.unmodifiableSet(tmp2);
			
			Set<Entry<String, String>> tmp3 = new HashSet<Map.Entry<String,String>>();
			tmp3.addAll(tmp1);
			tmp3.addAll(tmp2);
			ALL_PREFIXES = Collections.unmodifiableSet(tmp3);
			Map<String, String> tmp4 = new HashMap<String, String>();
			tmp4.put("Temperature","http://qudt.org/vocab/quantity#ThermodynamicTemperature");
			tmp4.put("Light","http://qudt.org/vocab/quantity#LuminousIntensity");
			FEATURES = Collections.unmodifiableMap(tmp4);
		}
		
		public static Set<Entry<String, String>> getPrefixSet(Ontology... ontologies){
			Set<Entry<String, String>> prefixSet = new HashSet<>();
			for(Ontology o : ontologies){
				prefixSet.add(new AbstractMap.SimpleEntry<String, String>(o.getPrefix(), "<"+o.getURI()+">"));
			}
			return prefixSet;
		}
}
