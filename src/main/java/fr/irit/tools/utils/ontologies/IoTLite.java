package fr.irit.tools.utils.ontologies;

public class IoTLite  extends Ontology{
	private static IoTLite instance;
	
	public static final String URI = "http://purl.oclc.org/NET/UNIS/fiware/iot-lite#";
	public static final String PREFIX = "iot-l";
	
	private IoTLite(){}
	
	public static IoTLite getInstance(){
		if(IoTLite.instance == null){
			IoTLite.instance = new IoTLite();
		}
		return IoTLite.instance;
	}
	
	/*
	 * Propserties
	 */
	public static final String hasUnit = URI+"hasUnit";
	public static final String hasQuantityKind = URI+"hasQuantityKind";

}
