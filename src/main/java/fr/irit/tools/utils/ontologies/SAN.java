package fr.irit.tools.utils.ontologies;

public class SAN extends Ontology{
	private static SAN instance;
	
	public static final String URI = "http://www.irit.fr/recherches/MELODI/ontologies/SAN#";
	public static final String PREFIX = "san";
	
	private SAN(){}
	
	public static SAN getInstance(){
		if(SAN.instance == null){
			SAN.instance = new SAN();
		}
		return SAN.instance;
	}
}
