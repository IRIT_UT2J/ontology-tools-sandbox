package fr.irit.tools.utils.ontologies;

public class DUL extends Ontology {
	private static DUL instance;
	
	public static final String URI = "http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#";
	public static final String PREFIX = "dul";
	
	/*
	 * Classes
	 */
	
	/*
	 * Properties 
	 */
	public static final String ASSOCIATED_WITH = URI+"AssociatedWith";
	
	private DUL(){}
	
	public static DUL getInstance(){
		if(DUL.instance == null){
			DUL.instance = new DUL();
		}
		return DUL.instance;
	}
}
