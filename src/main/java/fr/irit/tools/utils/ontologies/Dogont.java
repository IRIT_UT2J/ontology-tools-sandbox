package fr.irit.tools.utils.ontologies;

public class Dogont extends Ontology{
	private static Dogont instance;
	
	public static final String URI = "http://elite.polito.it/ontologies/dogont.owl#";
	public static final String PREFIX = "dogont";
	
	/*
	 * Classes
	 */
	public static final String ROOM = URI+"Room";
	public static final String FLOOR = URI+"Floor";
	public static final String THERMOSTAT = URI+"Thermostat";
	public static final String LAMP = URI+"Lamp";
	public static final String LIGHT_SENSOR = URI+"LightSensor";
	public static final String TEMPERATURE_SENSOR = URI+"TemperatureSensor";
	public static final String POWER_METER = URI+"PowerMeter";
	public static final String ACTIVE_POWER_METER = URI+"ActivePowerMeter";
	
	private Dogont(){}
	
	public static Dogont getInstance(){
		if(Dogont.instance == null){
			Dogont.instance = new Dogont();
		}
		return Dogont.instance;
	}
}
