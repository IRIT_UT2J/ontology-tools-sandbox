package fr.irit.tools.utils.ontologies;

public class OPABuilding extends Ontology {
	private static OPABuilding instance;
	
	public static final String URI 					= "https://w3id.org/laas-iot/adream-building#";
	public static final String PREFIX 				= "lab";
	
	private OPABuilding(){};
	public static OPABuilding getInstance(){
		if(OPABuilding.instance == null){
			OPABuilding.instance = new OPABuilding();
		}
		return OPABuilding.instance;
	}
}
