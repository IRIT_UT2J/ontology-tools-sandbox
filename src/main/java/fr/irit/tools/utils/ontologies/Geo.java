package fr.irit.tools.utils.ontologies;

public class Geo extends Ontology {
	private static Geo instance;
	
	public static final String URI 					= "http://www.w3.org/2003/01/geo/wgs84_pos#";
	public static final String PREFIX 				= "geo";
	
	
	/*
	 * Properties
	 */
	public static final String LONG					= URI+"long";
	public static final String LAT					= URI+"lat";
	public static final String LOCATION					= URI+"location";
	
	private Geo(){};
	public static Geo getInstance(){
		if(Geo.instance == null){
			Geo.instance = new Geo();
		}
		return Geo.instance;
	}
}
