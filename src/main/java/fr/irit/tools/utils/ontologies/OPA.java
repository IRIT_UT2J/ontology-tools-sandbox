package fr.irit.tools.utils.ontologies;

public class OPA extends Ontology {
	private static OPA instance;
	
	public static final String URI 					= "https://w3id.org/laas-iot/adream#";
	// Dirty hack to solve a content negotiation issue
	public static final String STATIC_PATH 			= "/home/nseydoux/dev/iot-ontologies/opa-common.owl";
	public static final String PREFIX 				= "opa";
	
	/*
	 * Concepts
	 */
	public static final String ADREAM_DEVICE	 	= URI+"ADREAMDevice";
	public static final String FAN_SPEED_CONTROL 	= URI+"Fan_speed_control";
	public static final String VENTILO_CONVECTOR 	= URI+"Fan_coil";
	public static final String COLD_WATER_VALVE 	= URI+"Cold_water_valve";
	public static final String HOT_WATER_VALVE 		= URI+"Hot_water_valve";
	public static final String SOLIVIA 				= URI+"Solivia";
	public static final String SMA 					= URI+"SMA";
	public static final String INVERTER_STRING 		= URI+"Inverter_string";
	public static final String SUNLIGHT_SENSOR 		= URI+"Sunlight_sensor";
	public static final String UTA					= URI+"AirProcessingUnit";
	public static final String PYRANOMETER			= URI+"Pyranometer";
	public static final String HYGROMETER			= URI+"Hygrometer";
	public static final String RAIN_GAUGE			= URI+"Rain_Gauge";
	public static final String WEATHERCOCK			= URI+"Weathercock";
	public static final String ANEMOMETER			= URI+"Anemometer";
	public static final String BAROMETER			= URI+"Barometer";
	public static final String OUTSIDE_TEMP			= URI+"OutsideTemperatureSensor";
	
	/*
	 * Properties
	 */
	public static final String IN_ROOM				= URI+"in_room";
	public static final String ON_FLOOR				= URI+"on_floor";
	public static final String IN_BUILDING			= URI+"in_building";
	public static final String VENTILATES			= URI+"ventilates";
	public static final String BELONGS_TO			= URI+"belongsToADREAMSubsystem";
	
	/*
	 * Individuals
	 */
	// The building
	public static final String ADREAM				= URI+"ADREAM";
	// Floors
	public static final String LEVEL_CRAWLSPACE		= URI+"Crawlspace";
	public static final String LEVEL_0				= URI+"Level_0";
	public static final String LEVEL_1				= URI+"Level_1";
	public static final String LEVEL_2				= URI+"Level_2";
	// Specific rooms
	public static final String GALLERY				= URI+"Gallery";
	public static final String EXTERIOR				= URI+"Exterior";
	// Adream subsystems
	public static final String ENERSubsystem		= URI+"ADREAMEnergySubsystem";
	public static final String LUMISubsystem		= URI+"ADREAMLightSubsystem";
	public static final String PHOTOVSubsystem		= URI+"ADREAMPhotovoltaicSubsystem";
	public static final String CVCSubsystem			= URI+"ADREAMTemperatureSubsystem";
	// Weather features
	public static final String AdreamWeather		= URI+"ADREAMWeather";
	public static final String AdreamHygrometry		= URI+"ADREAMHygrometry";
	public static final String AdreamPluviometry	= URI+"ADREAMPluviometry";
	public static final String AdreamPyranometry	= URI+"ADREAMPyranometry";
	public static final String AdreamOutTemperature	= URI+"ADREAMOutsideTemperature";
	public static final String AdreamWindSpeed		= URI+"ADREAMWindSpeed";
	public static final String AdreamWindDirection	= URI+"ADREAMWindDirection";
	public static final String AdreamAthmosPressure	= URI+"ADREAMAthmosphericPressure";
	// Observable properties
	public static final String Luminosity			= URI+"Luminosity";
	public static final String Temperature			= URI+"Temperature";
	public static final String ElectricalPower		= URI+"ElectricalPower";
	public static final String ElectricalTension	= URI+"ElectricalTension";
	public static final String ElectricalIntensity		= URI+"Electricalintensity";
	public static final String Sunlight				= URI+"Sunlight";
	public static final String Openness				= URI+"Openness";
	public static final String RotationSpeed		= URI+"RotationSpeed";
	// Other features
	public static final String GALLERY_TEMPERATURE	= URI+"GalleryTemperature";
	// Deployment
	public static final String ADREAM_TESTBED		= URI+"ADREAMTestbed";
	// Location
	public static final String ADREAM_COORDINATES	= URI+"ADREAMCoordinates";
	// Units
	public static final String Lux 					= URI+"Lux";
	public static final String Watt 				= URI+"Watt";
	public static final String Celsius 				= URI+"DegreeCelsius";
	public static final String MeterPerSecond 		= URI+"MeterPerSecond";
	public static final String Volt					= URI+"Volt";
	public static final String RadiationBySurface 	= URI+"WattPerSquareMetre";
	public static final String Pascal 				= URI+"Pascal";
	public static final String DegreeAngle 			= URI+"DegreeAngle";
	public static final String Millimetre 			= URI+"Millimetre";
	public static final String Percent 				= URI+"Percent";
	public static final String Ampere 				= URI+"Ampere";
	
	private OPA(){};
	public static OPA getInstance(){
		if(OPA.instance == null){
			OPA.instance = new OPA();
		}
		return OPA.instance;
	}
}
