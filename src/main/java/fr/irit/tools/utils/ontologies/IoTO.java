package fr.irit.tools.utils.ontologies;

public class IoTO  extends Ontology{
	private static IoTO instance;
	
	public static final String URI = "https://www.irit.fr/recherches/MELODI/ontologies/IoT-O#";
	public static final String PREFIX = "ioto";
	
	private IoTO(){}
	
	public static IoTO getInstance(){
		if(IoTO.instance == null){
			IoTO.instance = new IoTO();
		}
		return IoTO.instance;
	}
	
	/*
	 * Classes
	 */
	public static final String hasID = URI+"hasId";

}
