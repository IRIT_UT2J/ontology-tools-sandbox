package fr.irit.tools.utils.ontologies;

public class OPADataset extends Ontology{
	private static OPADataset instance;
	
	public static String URI = "https://w3id.org/laas-iot/adream/data#";
	public static String PREFIX = "opads";
	
	private OPADataset(){}
	
	public static OPADataset getInstance(){
		if(OPADataset.instance == null){
			OPADataset.instance = new OPADataset();
		}
		return OPADataset.instance;
	}

}
