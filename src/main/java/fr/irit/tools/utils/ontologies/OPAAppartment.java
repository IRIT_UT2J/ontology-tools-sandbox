package fr.irit.tools.utils.ontologies;

public class OPAAppartment extends Ontology {
	private static OPAAppartment instance;
	
	public static final String URI 					= "https://w3id.org/laas-iot/adream-appartment#";
	// Dirty hack to solve a content negotiation issue
	public static final String PREFIX 				= "appt";
	
	private OPAAppartment(){};
	public static OPAAppartment getInstance(){
		if(OPAAppartment.instance == null){
			OPAAppartment.instance = new OPAAppartment();
		}
		return OPAAppartment.instance;
	}
}
