package fr.irit.tools.utils.ontologies;

public class RDF extends Ontology{
	public static final String URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	
	public static final String TYPE = URI+"type";
	
	private RDF(){}
	private static RDF instance;
	public static RDF getInstance(){
		if(RDF.instance == null){
			RDF.instance = new RDF();
		}
		return RDF.instance;
	}
			
}
