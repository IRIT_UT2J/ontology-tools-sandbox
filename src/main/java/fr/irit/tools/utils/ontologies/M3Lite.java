package fr.irit.tools.utils.ontologies;

public class M3Lite  extends Ontology{
	private static M3Lite instance;
	
	public static final String URI = "http://purl.org/iot/vocab/m3-lite#";
	public static final String PREFIX = "m3-l";
	
	private M3Lite(){}
	
	public static M3Lite getInstance(){
		if(M3Lite.instance == null){
			M3Lite.instance = new M3Lite();
		}
		return M3Lite.instance;
	}
	
	/*
	 * Classes
	 */
	// Quantity kinds
	public static final String Illuminance = URI+"Illuminance";
	public static final String Energy = URI+"Energy";
	public static final String Temperature = URI+"Temperature";
	public static final String AirTemperature = URI+"AirTemperature";
	public static final String WindSpeed = URI+"WindSpeed";
	public static final String Tension = URI+"ElectricPotential";
	public static final String SolarRadiation = URI+"SolarRadiation";
	public static final String AtmosphericPressure = URI+"AtmosphericPressure";
	public static final String WindDirection = URI+"WindDirection";
	public static final String Rainfall = URI+"Rainfall";
	public static final String Humidity = URI+"Humidity";
	
	// Units
	public static final String Lux = URI+"Lux";
	public static final String Watt = URI+"Watt";
	public static final String Celsius = URI+"DegreeCelsius";
	public static final String MeterPerSecond = URI+"MetrePerSecond";
	public static final String Volt = URI+"Volt";
	public static final String RadiationBySurface = URI+"WattPerSquareMetre";
	public static final String Pascal = URI+"Pascal";
	public static final String DegreeAngle = URI+"DegreeAngle";
	public static final String Millimetre = URI+"Millimetre";
	public static final String Percent = URI+"Percent";
	
	
	
	
}
