package fr.irit.tools.utils.ontologies;

/**
 * This abstract class uses the Java reflexive API to access field values of 
 * its subclasses, each implementing a singleton.
 *
 */
public abstract class Ontology {
	
	public String getURI(){
		return this.getAttribute("URI");
	}
	public String getPrefix(){
		return this.getAttribute("PREFIX");
	}

	public String getAttribute(String att){
		Object o = new Object();
		try {
			return (String) this.getClass().getField(att).get(o);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
}
