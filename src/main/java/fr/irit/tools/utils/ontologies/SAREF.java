package fr.irit.tools.utils.ontologies;

public class SAREF extends Ontology {
	private static SAREF instance;
	
	public static final String URI = "http://w3id.com/saref#";
	public static final String PREFIX = "saref";
	
	private SAREF(){}
	
	public static SAREF getInstance(){
		if(SAREF.instance == null){
			SAREF.instance = new SAREF();
		}
		return SAREF.instance;
	}
}
