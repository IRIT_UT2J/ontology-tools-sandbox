package fr.irit.tools.utils.ontologies;

public class SSN extends Ontology {
	private static SSN instance;
	
	public static final String URI = "http://purl.oclc.org/NET/ssnx/ssn#";
	public static final String PREFIX = "ssn";
	
	/*
	 * Properties
	 */
	public static final String observes = 				SSN.URI+"observes";
	public static final String hasFeatureOfInterest =	SSN.URI+"featureOfInterest";
	public static final String hasAttachedSystem =	SSN.URI+"featureOfInterest";
	public static final String hasDeployment =	SSN.URI+"hasDeployment";
	public static final String onPlatform =	SSN.URI+"onPlatform";
	
	/*
	 * Classes
	 */
	public static final String platform = 				SSN.URI+"Platform";
	
	private SSN(){}
	
	public static SSN getInstance(){
		if(SSN.instance == null){
			SSN.instance = new SSN();
		}
		return SSN.instance;
	}
}
