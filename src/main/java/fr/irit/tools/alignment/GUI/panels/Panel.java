package fr.irit.tools.alignment.GUI.panels;

import javax.swing.*;

public class Panel extends JPanel {

    /** returns the value of attribute "results" */
    public String getSelect(){ return ""; }

    /** returns if a text field is empty */
    public boolean isEmpty(){ return true; }

    /** sets up the window */
    protected void initComponents() {}
}
