package fr.irit.tools.alignment.GUI;

import java.io.PrintWriter;

public class GUIFactory {

    public enum TypeWindows{
        LAUNCH,
        MENU,
        TYPESET,
        PREFIX,
        SIMPLE,
        STRUCTSET,
        COMPLEX
    }

    public static Gui createGui(TypeWindows typeWindows) {
        return switchCreate(typeWindows, null, 0);
    }
    public static Gui createGui(TypeWindows typeWindows, PrintWriter out) {return switchCreate(typeWindows, out, 0);}
    public static Gui createGui(TypeWindows typeWindows, PrintWriter out, int type) {return switchCreate(typeWindows, out, type);}

    private static Gui switchCreate(TypeWindows typeWindows, PrintWriter out, int type) {
        switch(typeWindows) {
            case LAUNCH   :   return new IntroGUI();
            case MENU : return new MenuGUI();
            case TYPESET : return new TypeSet();
            case PREFIX: return new PrefixGUI(out);
            case SIMPLE: return new SimpleGUIMod(out, type);
            case STRUCTSET : return new StructComplexSet();
            case COMPLEX: return new ComplexGUIMod(out, type);
            default :
                System.out.println("Element de GUI inexistant !");
                return null;
        }
    }
}
