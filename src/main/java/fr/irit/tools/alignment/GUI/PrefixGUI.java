package fr.irit.tools.alignment.GUI;


import fr.irit.tools.alignment.GUI.panels.AutoBoxSet;
import fr.irit.tools.alignment.edoaltypes.PrefixEdoal;
import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

public class PrefixGUI extends Gui {
    private AutoBoxSet uriList;
    private JTextField prefix;
    private PrintWriter out;

    PrefixGUI(PrintWriter out) {
        this.out = out;
        initComponents();
    }

    /** creates a new auto-completion folding list with all the ontologies URI */
    private AutoBoxSet FillList(){
        return new AutoBoxSet(Controller.getInstance().getAllURI(), 0);
    }

    private void WriteInFile(String uri, String prefix){
        PrefixEdoal pfx = new PrefixEdoal(uri, prefix);
        out.println(pfx.WritePrefix() + "\n");
        //System.out.println(pfx.WritePrefix());
    }

    /** sets up the window */
    protected void initComponents() {
        JLabel turi = new JLabel("Select URI from the ontologies : ");
        JLabel tprefix = new JLabel("Choose a prefix for this URI : ");
        error = new JLabel(" ");
        prefix = new JTextField();
        uriList = FillList();

        JButton generate = new JButton("Generate");
        generate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (prefix.getText().isEmpty()){
                    notif("<html><font color='red'>Must input a prefix.</font></html>");
                }
                else {
                    //System.out.println("generate");
                    Controller.getInstance().setPrefixForOnt(uriList.getItemForPrefix(), prefix.getText());
                    WriteInFile(uriList.getItemForPrefix(), prefix.getText());
                    notif("<html><font color='green'>Success !</font></html>");
                }
            }
        });

        this.setTitle("Prefix");
        this.setResizable(false);
        this.setLayout(new GridLayout(6, 1));
        this.setPreferredSize(new Dimension(600, 150));
        this.add(turi);
        this.add(uriList);
        this.add(tprefix);
        this.add(prefix);
        this.add(error);
        this.add(generate);

        // packs the fenetre: size is calculated
        // regarding the added components
        this.pack();
        // the JFrame is visible now
        this.setVisible(true);

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}

