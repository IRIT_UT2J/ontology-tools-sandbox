package fr.irit.tools.alignment.GUI.panels;

import fr.irit.tools.alignment.main.Controller;

public class PanelFactory {
    public enum TypePanel{
        CELL,
        FOOTER,
        SIMPLE,
        CAT
    }

    public static Panel createPanel(TypePanel typePanel) {return switchCreate(typePanel, 0, 0);}
    public static Panel createPanel(TypePanel typePanel, int numOfOnt) {return switchCreate(typePanel, numOfOnt, 0);}
    public static Panel createPanel(TypePanel typePanel, int numOfOnt, int box) {return switchCreate(typePanel, numOfOnt, box);}

    private static Panel switchCreate(TypePanel typePanel, int numOfOnt, int box) {
        switch(typePanel) {
            case CELL : return new PanelCell();
            case FOOTER : return new PanelFooter();
            case SIMPLE : return new PanelSimple(numOfOnt, new AutoBoxSet(Controller.getInstance().GenerateList(numOfOnt, box), 0));
            case CAT : return new PanelCAT(numOfOnt);
            default :
                System.out.println("Panel de GUI inexistant !");
                return null;
        }
    }
}
