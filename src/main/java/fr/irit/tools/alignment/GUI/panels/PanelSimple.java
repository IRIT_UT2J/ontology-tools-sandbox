package fr.irit.tools.alignment.GUI.panels;

import javax.swing.*;
import java.awt.*;

public class PanelSimple extends Panel {
    private int number;
    private AutoBoxSet fold;

    PanelSimple(int number, AutoBoxSet fold){
        this.number = number;
        this.fold = fold;
        initComponents();
    }

    /** returns the selected item */
    public String getSelect(){ return fold.getItem(); }

    protected void initComponents() {
        JLabel ent1 = new JLabel("< ENTITY " + number + " >");
        JLabel ent2 = new JLabel("</ ENTITY " + number + " >");
        JLabel text = new JLabel("Select an entity URI : ");

        this.setLayout(new GridLayout(4, 1));
        this.setPreferredSize(new Dimension(900, 130));
        this.add(ent1);
        this.add(text);
        this.add(fold);
        this.add(ent2);
    }
}
