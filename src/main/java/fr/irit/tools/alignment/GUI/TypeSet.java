package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TypeSet extends Gui {
    private JComboBox foldingType;

    TypeSet() {
        initComponents();
    }

    /** returns the int-code of the selected item (prop) in the folding list */
    private int typeOfProp(){
        String type = (String)foldingType.getSelectedItem();
        switch (type){
            case "classes":
                return 1;
            case "object properties":
                return 2;
            case "data properties":
                return 3;
            default:
                System.out.println("error in type of corresp");
                return ' ';
        }
    }

    protected void initComponents() {
        JLabel corresp = new JLabel("Correspondences between ");
        JLabel exemple = new JLabel(new ImageIcon(this.getClass().getResource("/images/edoalsimple.jpg")));

        foldingType = new JComboBox();
        foldingType.addItem("classes");
        foldingType.addItem("object properties");
        foldingType.addItem("data properties");

        JButton ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("type");
                Controller.getInstance().getSimple(typeOfProp());
                close();
            }
        });

        this.setLayout(new BorderLayout());
        this.add(corresp, BorderLayout.WEST);
        this.add(foldingType, BorderLayout.CENTER);
        this.add(ok, BorderLayout.EAST);
        this.add(exemple, BorderLayout.SOUTH);
        this.setTitle("Alignments type");
        this.setResizable(false);

        this.pack();
        // the JFrame is visible now
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
