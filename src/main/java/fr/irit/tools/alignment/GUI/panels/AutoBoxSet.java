package fr.irit.tools.alignment.GUI.panels;

import fr.irit.tools.alignment.autocompletion.Java2sAutoComboBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;

public class AutoBoxSet extends JPanel {
    private Java2sAutoComboBox entity;
    private ArrayList<String> list;
    private int nbOfButton;

    /**
     * creates a folding list with auto-completion
     * @param list
     * @param n
     */
    public AutoBoxSet(ArrayList<String> list, int n) {
        this.list = list;
        OrderList();
        this.nbOfButton = n;
        initComponents();
    }

    /**
     * sorts the list in alphabetical order
     */
    private void OrderList(){
        // Sorting
        list.sort(new Comparator<String>() {
            public int compare(String uri1, String uri2) {
                return uri1.toLowerCase().compareTo(uri2.toLowerCase());
            }
        });
    }

    /**
     * returns only the URI from the selected item in list
     * @return
     */
    public String getItem(){
        String uri = (String)entity.getSelectedItem();
        uri = uri.substring(uri.indexOf("(")+1, uri.lastIndexOf(")"));
        return uri;
    }

    /**
     * returns the selected item (case item=URI)
     * @return
     */
    public String getItemForPrefix(){
        return (String)entity.getSelectedItem();
    }

    /**
     * sets up the window
     */
    private void initComponents() {
        if (nbOfButton == 0) {
            entity = new Java2sAutoComboBox(list);

            this.setLayout(new GridLayout(1, 2));
            this.add(entity);
        }
        else {
            entity = new Java2sAutoComboBox(list);

            JButton ok = new JButton("OK");
            ok.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println(entity.getSelectedItem());
                }
            });

            this.setLayout(new GridLayout(1, 2));
            this.setPreferredSize(new Dimension(1000, 30));
            this.add(entity);
        }
    }
}
