package fr.irit.tools.alignment.GUI.panels;

import javax.swing.*;
import java.awt.*;

public class PanelCell extends Panel {
    private JTextField cell;

    PanelCell(){
        initComponents();
    }

    /** returns text in cell */
    public String getSelect(){ return cell.getText(); }

    /** returns whether cell is empty */
    public boolean isEmpty(){ return cell.getText().isEmpty(); }

    protected void initComponents() {
        JLabel tcell = new JLabel("Name of the Cell");
        cell = new JTextField();

        this.setLayout(new GridLayout(2,1));
        this.setPreferredSize(new Dimension(900, 50));
        this.add(tcell);
        this.add(cell);
    }
}
