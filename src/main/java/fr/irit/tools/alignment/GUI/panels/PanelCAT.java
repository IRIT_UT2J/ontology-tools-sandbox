package fr.irit.tools.alignment.GUI.panels;

import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;

public class PanelCAT extends Panel {
    private AutoBoxSet class1, class2, prop;
    private int number;

    PanelCAT(int number){
        this.number = number;
        initComponents();
    }

    /** returns a string containing all 3 autobox selected items */
    public String getSelect(){
        return class1.getItem() + " " + prop.getItem() + " " + class2.getItem();
    }

    protected void initComponents() {
        JLabel ent1 = new JLabel("< ENTITY " + number + " >");
        JLabel ent2 = new JLabel("</ ENTITY " + number + " >");

        JLabel tclass1 = new JLabel("Select the Domain Class :");
        JLabel tclass2 = new JLabel("Select the Range Class :");
        JLabel tprop = new JLabel("Select the concerned Property :");

        class1 = new AutoBoxSet(Controller.getInstance().GenerateList(number, 1), 0);
        class2 = new AutoBoxSet(Controller.getInstance().GenerateList(number, 1), 0);
        prop = new AutoBoxSet(Controller.getInstance().GenerateList(number, 2), 0);

        this.setLayout(new GridLayout(8, 1));
        this.setPreferredSize(new Dimension(900, 300));
        this.add(ent1);
        this.add(tclass1);
        this.add(class1);
        this.add(tprop);
        this.add(prop);
        this.add(tclass2);
        this.add(class2);
        this.add(ent2);
    }
}
