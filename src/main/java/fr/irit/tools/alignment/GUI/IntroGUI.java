package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IntroGUI extends Gui {
    private JTextField ont1, ont2;

    public IntroGUI(){
        initComponents();
    }

    /** Initializes the Fenetre components */
    protected void initComponents() {

        JLabel tont1 = new JLabel("Input URI of the first ontology. ");
        JLabel tont2 = new JLabel("Input URI of the second ontology. ");
        error = new JLabel(" ");
        ont1 = new JTextField();
        ont2 = new JTextField();

        //FOR TESTING PURPOSES!!!
        ont1.setText("https://www.irit.fr/recherches/MELODI/ontologies/IoT-O.owl");
        ont2.setText("http://ontology.fiesta-iot.eu/ontologyDocs/fiesta-iot.owl");
        //FOR TESTING PURPOSES!!!

        JButton launch = new JButton("Ready to align");
        launch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("launch");

                if (ont1.getText().isEmpty() || ont2.getText().isEmpty()){
                    notif("<html><font color='red'>Please choose two ontologies to align.</font></html>");
                }
                else if (!Controller.getInstance().isURI(ont1.getText()) || !Controller.getInstance().isURI(ont2.getText())){
                    notif("<html><font color='red'>Please enter two correct URI.</font></html>");
                }
                else{
                    notif("<html><font color='green'>Loading...</font></html>");
                    Controller.getInstance().setUri1(ont1.getText());
                    Controller.getInstance().setUri2(ont2.getText());
                    Controller.getInstance().getMenu(ont1.getText(), ont2.getText());
                    close();
                }
            }
        });

        this.setTitle("Launcher");
        this.setResizable(false);
        this.setLayout(new GridLayout(3,1));
        this.setPreferredSize(new Dimension(400, 200));
        this.add(getPanel(tont1,ont1));
        this.add(getPanel(tont2,ont2));
        this.add(getPanel(error, launch));

        // packs the fenetre: size is calculated
        // regarding the added components
        this.pack();
        // the JFrame is visible now
        this.setVisible(true);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
