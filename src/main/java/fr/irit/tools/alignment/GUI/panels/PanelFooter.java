package fr.irit.tools.alignment.GUI.panels;

import javax.swing.*;
import java.awt.*;

public class PanelFooter extends Panel {
    private JComboBox relation;
    private JTextField measure;

    PanelFooter(){
        initComponents();
    }

    private String getRelation(){
        String rel = (String)relation.getSelectedItem();
        return rel.substring(rel.indexOf("(")+1, rel.indexOf(")"));
    }

    /** returns concatenation of value in measure and selected item for relation */
    public String getSelect(){
        if (this.isEmpty()){
            return this.getRelation();
        }
        else{
            return this.getRelation() + " " + measure.getText();
        }
    }

    /** returns whether measure is empty */
    public boolean isEmpty(){ return measure.getText().isEmpty(); }

    protected void initComponents() {
        relation = new JComboBox();
        relation.addItem("= (Equivalence)");
        relation.addItem("< (SubsumedBy)");
        relation.addItem("> (Subsumes)");

        measure = new JTextField();
        JLabel trelation = new JLabel("Relation between the entities : ");
        JLabel tmeasure = new JLabel("Measure of the relation (optional) : ");

        this.setLayout(new GridLayout(4,1));
        this.setPreferredSize(new Dimension(900, 100));
        this.add(trelation);
        this.add(relation);
        this.add(tmeasure);
        this.add(measure);
    }
}
