package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuGUI extends Gui {
    private JButton simple;
    private JButton complex;
    private JButton prefix;
    private JButton validate;
    private JTextField file;
    private JLabel tfile, talig, tpfx;

    MenuGUI() {
        initComponents();
    }

    protected JPanel generatePanel(){
        JPanel form = new JPanel(new GridLayout(6,1));
        form.add(validate);
        form.add(tpfx);
        form.add(prefix);
        form.add(talig);
        form.add(simple);
        form.add(complex);
        return form;
    }

    protected void initComponents() {

        tfile = new JLabel("Please choose your output file.");
        tpfx = new JLabel("Add prefix for your ontology URI.");
        talig = new JLabel(" Choose the type of alignment to generate. ");
        JLabel texit = new JLabel("<html><font color='red'> Press EXIT to generate your file and quit the application. </font></html>");
        file = new JTextField();

        //FOR TESTING PURPOSES!!!
        //file.setText("test.txt");
        //FOR TESTING PURPOSES!!!

        validate = new JButton("Confirm output file");
        validate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("validate");

                if (!file.getText().isEmpty()){
                    Controller.getInstance().ChooseFile(file.getText());
                    prefix.setEnabled(true);
                    simple.setEnabled(true);
                    complex.setEnabled(true);
                    tfile.setText("Please choose your output file.");
                }
                else{
                    tfile.setText("<html><font color='red'>Please choose your output file !</font></html>");
                }
            }
        });

        prefix = new JButton("Prefix for URI");
        prefix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("prefix");
                Controller.getInstance().getPrefix();
            }
        });

        simple = new JButton("Simple Alignment");
        simple.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("simple");
                Controller.getInstance().getTypeSet();
            }
        });

        complex = new JButton("Complex Alignment");
        complex.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("complex");
                Controller.getInstance().getStructSet();
            }
        });

        JButton exit = new JButton("EXIT");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("exit");
                Controller.getInstance().Exit();
            }
        });

        // before an output file is chosen
        prefix.setEnabled(false);
        simple.setEnabled(false);
        complex.setEnabled(false);

        this.setTitle("Menu");
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        this.add(getPanel(tfile,file), BorderLayout.NORTH);
        this.add(generatePanel(), BorderLayout.CENTER);
        this.add(getPanel(texit,exit), BorderLayout.SOUTH);

        // packs the fenetre: size is calculated
        // regarding the added components
        this.pack();
        // the JFrame is visible now
        this.setVisible(true);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
