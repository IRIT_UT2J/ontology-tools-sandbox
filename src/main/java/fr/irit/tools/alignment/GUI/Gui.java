package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.GUI.panels.AutoBoxSet;
import fr.irit.tools.alignment.autocompletion.Java2sAutoComboBox;
import fr.irit.tools.alignment.main.Controller;
import javax.swing.*;
import java.awt.*;

public class Gui extends JFrame{
    protected JLabel error;

    /** sets up the window */
    protected void initComponents() {}

    /** generates a panel for the window */
    protected JPanel generatePanel(){ return new JPanel(); }

    /** sets the notification text */
    protected void notif(String msg) {
        error.setText(msg);
    }

    /** creates a new auto-completion folding list */
    protected AutoBoxSet FillList(int type, int ont, int nbButton){
        return new AutoBoxSet(Controller.getInstance().GenerateList(ont, type), nbButton);
    }

    /** creates a panel with a label and a folding list and returns it */
    protected JPanel getPanel(JLabel label, JComboBox box){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,1));
        panel.add(label);
        panel.add(box);
        return panel;
    }

    /** creates a panel with a label and a jtextfield and returns it */
    protected JPanel getPanel(JLabel label, JTextField field){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,1));
        panel.add(label);
        panel.add(field);
        return panel;
    }

    /** creates a panel with a label and a jtextfield and returns it */
    protected JPanel getPanel(JLabel label, JButton button){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,1));
        panel.add(label);
        panel.add(button);
        return panel;
    }

    /** creates a panel with a label and a jtextfield and returns it */
    protected JPanel getPanel(JButton button1, JButton button2){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,1));
        panel.add(button1);
        panel.add(button2);
        return panel;
    }

    /** creates a panel with a button and a folding list on the same line and returns it */
    protected JPanel TwoOnOneLine(JButton butt, Java2sAutoComboBox box){
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        panel.add(box);
        panel.add(butt);
        return panel;
    }

    /** closes the window */
    public void close(){
        this.setVisible(false);
        this.dispose();
    }
}
