package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StructComplexSet extends Gui {
    private JComboBox typeEnt1, typeEnt2, dpList;

    StructComplexSet() {
        initComponents();
    }

    /** fills a folding list with input string and returns it */
    private JComboBox setFoldingList(String entry){
        String split[] = entry.split(";");
        JComboBox box = new JComboBox();

        for (String aSplit : split) {
            box.addItem(aSplit);
        }
        return box;
    }

    protected JPanel getPanel(JLabel label, JComboBox box){
        JPanel panel = new JPanel();
        panel.add(label);
        panel.add(box);
        return panel;
    }

    /**
     * chooses parameters for the next complex window creation
     * according to the selected pattern and the type for URI
     * (1: simple; 2: cat)
     * (order=0 for simple/composed -- order=1 for composed/simple) */
    private void inferNextWindow(String pattern, int order){

        switch (pattern){
            case "CAT" :
                if (order == 0) {
                    Controller.getInstance().getComplex(12);
                }
                else {
                    Controller.getInstance().getComplex(21);
                }
                close();
                break;
            case "TBD" :
                notif("<html><font color='green'>~ TBD = To Be Determined ~</font></html>");
                break;
            default:
                notif("<html><font color='red'>No.</font></html>");
                break;
        }
    }

    protected void initComponents() {
        JLabel exemple = new JLabel(new ImageIcon(this.getClass().getResource("/images/edoalcomplex.jpg")));

        error = new JLabel("");
        JLabel te1 = new JLabel("Entity 1 : < " + Controller.getInstance().getUri1() + " > \tis ");
        JLabel te2 = new JLabel("Entity 2 : < " + Controller.getInstance().getUri2() + " >  \tis ");
        JLabel tdp = new JLabel("Select a Complex Design Pattern :");

        typeEnt1 = setFoldingList("simple;composed");
        typeEnt2 = setFoldingList("composed;simple");
        dpList = setFoldingList("CAT;TBD");

        JButton ok = new JButton("Validate");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String t1 = (String)typeEnt1.getSelectedItem();
                String t2 = (String)typeEnt2.getSelectedItem();
                String pattern = (String)dpList.getSelectedItem();

                //System.out.println(t1 + " " + t2);

                if (t1.equals("simple") && t2.equals("simple")){
                    Controller.getInstance().getTypeSet();
                    close();
                }
                else if (t1.equals("simple") && t2.equals("composed")){
                    inferNextWindow(pattern, 0);
                }
                else if (t1.equals("composed") && t2.equals("simple")){
                    inferNextWindow(pattern, 1);
                }
                else {
                    // t1 = t2 = composed
                    notif("<html><font color='red'>Sorry, this format is not supported so far...</font></html>");
                }
            }
        });


        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 1));
        panel.add(getPanel(te1,typeEnt1));
        panel.add(getPanel(te2,typeEnt2));
        panel.add(getPanel(tdp,dpList));
        panel.add(error);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.NORTH);
        this.add(ok, BorderLayout.CENTER);
        this.add(exemple, BorderLayout.SOUTH);

        this.setTitle("Structures of entities");
        this.setResizable(false);

        this.pack();
        // the JFrame is visible now
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
