package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.GUI.panels.*;
import fr.irit.tools.alignment.GUI.panels.Panel;
import fr.irit.tools.alignment.edoaltypes.SimpleEdoal;
import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

public class SimpleGUIMod extends Gui {
    private int type;
    private PrintWriter out;

    /** creates the simple gui */
    SimpleGUIMod(PrintWriter out, int type) {
        this.out = out;
        this.type = type;
        initComponents();
    }

    private void WriteInFile(String cell, String ent1, String ent2, String relation, double measure){
        SimpleEdoal simple;
        if (measure < 0.0) {
            simple = new SimpleEdoal(cell, ent1, ent2, relation, type);
        }
        else {
            simple = new SimpleEdoal(cell, ent1, ent2, relation, measure, type);
        }
        //System.out.println(simple);
        out.println(simple.WriteSimple() + "\n");
    }

    /** sets up the window */
    protected void initComponents() {
        error = new JLabel(" ");

        Panel cell = PanelFactory.createPanel(PanelFactory.TypePanel.CELL);
        Panel footer = PanelFactory.createPanel(PanelFactory.TypePanel.FOOTER);
        Panel ent1 = PanelFactory.createPanel(PanelFactory.TypePanel.SIMPLE, 1, type);
        Panel ent2 = PanelFactory.createPanel(PanelFactory.TypePanel.SIMPLE, 2, type);

        JButton generate = new JButton("Generate");
        generate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("generate");
                notif(" ");
                String check1 = Controller.getInstance().CheckPrefixForOnt(ent1.getSelect());
                String check2 = Controller.getInstance().CheckPrefixForOnt(ent2.getSelect());

                if (cell.isEmpty()){
                    notif("<html><font color='red'>Must input a cell name.</font></html>");
                }
                else if (footer.isEmpty()) {
                    WriteInFile(cell.getSelect(), check1, check2, footer.getSelect(), -1.0);
                    notif("<html><font color='green'>Success !</font></html>");
                }else{
                    try {
                        String full[] = footer.getSelect().split(" ");
                        double m = Double.parseDouble(full[1]);
                        WriteInFile(cell.getSelect(), check1, check2, full[0], m);
                        notif("<html><font color='green'>Success !</font></html>");
                    }catch(NumberFormatException e1) {
                        notif("<html><font color='red'>Measure must be a float (between 0.0 and 1.0).</font></html>");
                    }
                }

            }
        });

        this.setTitle("Simple Alignment");
        this.setResizable(false);

        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(cell, BorderLayout.NORTH);
        p1.add(ent1, BorderLayout.CENTER);
        p1.add(ent2, BorderLayout.SOUTH);

        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(footer, BorderLayout.NORTH);
        p2.add(getPanel(error, generate), BorderLayout.CENTER);

        this.setLayout(new BorderLayout());
        this.add(p1, BorderLayout.CENTER);
        this.add(p2, BorderLayout.SOUTH);

        // packs the fenetre: size is calculated
        // regarding the added components
        this.pack();
        // the JFrame is visible now
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
