package fr.irit.tools.alignment.GUI;

import fr.irit.tools.alignment.GUI.panels.Panel;
import fr.irit.tools.alignment.GUI.panels.PanelFactory;
import fr.irit.tools.alignment.edoaltypes.ComplexEdoal;
import fr.irit.tools.alignment.main.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

public class ComplexGUIMod extends Gui {
    private int order;
    private PrintWriter out;

    ComplexGUIMod(PrintWriter out, int order){
        this.order = order;
        this.out = out;
        initComponents();
    }

    /** chooses the appropriate complex panel to come */
    private Panel inferPanel(int typePanel, int ont, int type){
        switch (typePanel) {
            case 1:
                return PanelFactory.createPanel(PanelFactory.TypePanel.SIMPLE, ont, type);
            case 2:
                return PanelFactory.createPanel(PanelFactory.TypePanel.CAT, ont);
            default:
                notif("Panel does not exist");
                break;
        }
        return null;
    }

    /** writes the complex alignment in file */
    private void WriteInFile(int order, String cell, String class1, String class2, String attribute2, String result2, String relation, double measure){
        ComplexEdoal complex;
        if (measure < 0.0) {
            complex = new ComplexEdoal(cell, class1, class2, attribute2, result2, relation);
        }
        else {
            complex = new ComplexEdoal(cell, class1, class2, attribute2, result2, relation, measure);
        }
        //System.out.println(complex);
        out.println(complex.WriteComplex(order) + "\n");
    }

    protected void initComponents() {
        error = new JLabel(" ");

        Panel cell = PanelFactory.createPanel(PanelFactory.TypePanel.CELL);
        Panel footer = PanelFactory.createPanel(PanelFactory.TypePanel.FOOTER);
        Panel ent1 = inferPanel(order/10, 1, 1);
        Panel ent2 = inferPanel(order%10, 2, 1);

        JButton generate = new JButton("Generate");
        generate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("generate");
                notif(" ");
                //String rel = (String) foldingList.getSelectedItem();

                if (cell.isEmpty()) {
                    notif("<html><font color='red'>Must input a cell name.</font></html>");
                }
                else {
                    //to construct the complex edoal string (1: simple; 2: cat)
                    //i know it's awful, but it's late and rainy...
                    String c1, c2, a2, r2;
                    if (order == 12) {
                        c1 = Controller.getInstance().CheckPrefixForOnt(ent1.getSelect());
                        String full[] = ent2.getSelect().split(" ");
                        c2 = Controller.getInstance().CheckPrefixForOnt(full[0]);
                        a2 = Controller.getInstance().CheckPrefixForOnt(full[1]);
                        r2 = Controller.getInstance().CheckPrefixForOnt(full[2]);
                    } else if (order == 21) {
                        c1 = Controller.getInstance().CheckPrefixForOnt(ent2.getSelect());
                        String full[] = ent1.getSelect().split(" ");
                        c2 = Controller.getInstance().CheckPrefixForOnt(full[0]);
                        a2 = Controller.getInstance().CheckPrefixForOnt(full[1]);
                        r2 = Controller.getInstance().CheckPrefixForOnt(full[2]);
                    } else {
                        c1 = "";
                        c2 = "";
                        a2 = "";
                        r2 = "";
                    }

                    if (footer.isEmpty()) {
                        WriteInFile(order, cell.getSelect(), c1, c2, a2, r2, footer.getSelect(), -1.0);
                        notif("<html><font color='green'>Success !</font></html>");
                    } else {
                        try {
                            String full[] = footer.getSelect().split(" ");
                            double m = Double.parseDouble(full[1]);
                            WriteInFile(order, cell.getSelect(), c1, c2, a2, r2, full[0], m);
                            notif("<html><font color='green'>Success !</font></html>");
                        } catch (NumberFormatException e1) {
                            notif("<html><font color='red'>Measure must be a float (between 0.0 and 1.0).</font></html>");
                        }
                    }
                }
            }
        });

        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(cell, BorderLayout.NORTH);
        p1.add(ent1, BorderLayout.CENTER);
        p1.add(ent2, BorderLayout.SOUTH);

        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(footer, BorderLayout.NORTH);
        p2.add(getPanel(error, generate), BorderLayout.CENTER);

        this.setTitle("Complex Alignment");
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        this.add(p1, BorderLayout.CENTER);
        this.add(p2, BorderLayout.SOUTH);

        // packs the fenetre: size is calculated
        // regarding the added components
        this.pack();
        // the JFrame is visible now
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
