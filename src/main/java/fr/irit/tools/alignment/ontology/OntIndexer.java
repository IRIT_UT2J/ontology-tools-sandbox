package fr.irit.tools.alignment.ontology;

import fr.irit.tools.utils.ModelLoader;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RiotException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class OntIndexer {
    private HashMap<String, ArrayList<String>> classes;
    private HashMap<String, ArrayList<String>> objproperty;
    private HashMap<String, ArrayList<String>> dataproperty;
    private ArrayList<String> listImport;

    public OntIndexer(String ont) {
        this.classes = new HashMap<>();
        this.objproperty = new HashMap<>();
        this.dataproperty = new HashMap<>();
        this.listImport = new ArrayList<>();

        this.ConstructIndex(ont);
    }

    /** returns the list of all imported URIs */
    public ArrayList<String> getListImport() {
        return listImport;
    }

    /** returns a keyset from one of the hashmaps depending on the imput int-code
     * @param type
     * @return
     */
    public Set<String> getSet(int type){
        switch (type){
            case 1:
                return classes.keySet();
            case 2:
                return objproperty.keySet();
            case 3:
                return dataproperty.keySet();
            default:
                System.out.println("No keyset available for input type");
                break;
        }
        return null;
    }

    /**
     * returns a lists of URI from one of the hashmap depending on the given keyset
     * @param key
     * @param type
     * @return
     */
    public ArrayList<String> getListURI(String key, int type){
        switch (type){
            case 1:
                return classes.get(key);
            case 2:
                return objproperty.get(key);
            case 3:
                return dataproperty.get(key);
            default:
                System.out.println("No object available for this key");
                break;
        }
        return null;
    }

    /**
     * tests if an input String is a URI
     * @param string
     * @return
     */
    private boolean isURI(String string){
        //without it some strange results like :
        //-2f35f8f7:15cd42521cf:-7e23 [-2f35f8f7:15cd42521cf:-7e23]
        //-2f35f8f7:15cd42521cf:-7e26 [-2f35f8f7:15cd42521cf:-7e26]
        return (string.startsWith("http://") || string.startsWith("https://"));
    }

    /**
     * indexes the URIs of the specified list into the right index
     * @param listURI
     * @param index
     */
    private void index(ArrayList<String> listURI, HashMap<String, ArrayList<String>> index){
        for (String uri : listURI) {
            if (isURI(uri)) {
                String key;
                String uriToAdd;

                if (uri.contains("#")) {
                    key = uri.substring(uri.indexOf('#') + 1, uri.length());
                    uriToAdd = uri.substring(0, uri.indexOf("#")+1);
                } else {
                    int pos = uri.lastIndexOf("/");
                    key = uri.substring(pos + 1, uri.length());
                    uriToAdd = uri.substring(0, pos+1);
                }

                if (!listImport.contains(uriToAdd)){
                    listImport.add(uriToAdd);
                    //System.out.println(uriToAdd);
                }

                if (!index.containsKey(key)) {
                    index.put(key, new ArrayList<>());
                }
                if (!index.get(key).contains(uri)) {
                    index.get(key).add(uri);
                }
            }
        }
    }

    /**
     * recursively construct the 3 indexes of the given ontology
     * and the ontologies it imports
     * @param ont
     */
    private void ConstructIndex(String ont){
        try {
            Model m = ModelLoader.getLoadedModel(ont);
            index(OntReader.getObjectProp(m), objproperty);
            index(OntReader.getDataProp(m), dataproperty);
            index(OntReader.getClasses(m), classes);

            //System.out.println("bonjour");
            ArrayList<String> imports = OntReader.getImport(m);
            for (String importOnt : imports) {
                if (!this.listImport.contains(importOnt)) {
                    //System.out.println(importOnt);
                    this.listImport.add(importOnt);
                    ConstructIndex(importOnt);
                }
            }
            //System.out.println("a plus");

        } catch (RiotException e) {
            //e.printStackTrace();
        }
    }

    /**
     * prints the specied index of this class
     * @param index
     */
    private String printIndex(HashMap<String, ArrayList<String>> index){
        //System.out.println("SIZE : " + index.size());
        String output = "SIZE : " + index.size() + "\n";
        for (String key: index.keySet()){
            ArrayList<String> value = index.get(key);
            //System.out.println(key + " " + value);
            output += key + " " + value + "\n";
        }
        System.out.println();
        return output;
    }

    public String toString(){
        return ("\n=================================OBJECT PROP==================================\n" +
                printIndex(objproperty) +
                "\n==================================DATA PROP===================================\n" +
                printIndex(dataproperty) +
                "\n===================================CLASSES====================================\n" +
                printIndex(classes));
    }
}
