package fr.irit.tools.alignment.ontology;

import fr.irit.tools.queries.QueryEngine;
import org.apache.jena.rdf.model.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OntReader {

    /**
     * prints the result of the query that generated the map
     * (shows what's inside the map)
     * @param map
     */
    private static void queryResults(List<Map<String, String>> map){
        System.out.println("SIZE : " + map.size());
        if (!map.isEmpty()) {
            for (Map<String, String> m : map) {
                Set<String> set = m.keySet();
                for (String key : set) {
                    System.out.println("key:" + key + " obj:" + m.get(key));
                }
            }
        }
        System.out.println();
    }

    /**
     * launch a sparql query on the specified model
     * returns a list of all the requested URI
     * @param query
     * @param model
     * @return
     */
    private static ArrayList<String> listURI(String query, Model model){
        List<Map<String, String>> map = QueryEngine.selectQuery(query, model);
        //queryResults(map);

        ArrayList<String> list = new ArrayList<>();
        if (map != null && !map.isEmpty()) {
            for (Map<String, String> m : map) {
                Set<String> set = m.keySet();
                for (String key : set) {
                    list.add(m.get(key));
                }
            }
        }
        return list;
    }

    /**
     * returns a list of ObjectProperty URIs in model
     * @param m
     * @return
     */
    public static ArrayList<String> getObjectProp(Model m){
        String queryObjProp =
                        "PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
                        "SELECT ?objProp " +
                        "WHERE { ?objProp a owl:ObjectProperty. }";

        return listURI(queryObjProp, m);
    }

    /**
     * returns a list of DatatypeProperty URIs in model
     * @param m
     * @return
     */
    public static ArrayList<String> getDataProp(Model m){
        String queryDataProp =
                        "PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
                        "SELECT ?dataProp " +
                        "WHERE { ?dataProp a owl:DatatypeProperty. }";

        return listURI(queryDataProp, m);
    }

    /**
     * returns a list of Classes URIs in model
     * @param m
     * @return
     */
    public static ArrayList<String> getClasses(Model m){
        String queryClasses =
                        "PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
                        "SELECT ?class " +
                        "WHERE { ?class a owl:Class. }";

        return listURI(queryClasses, m);
    }

    /**
     * returns a list of imported ontologies URIs in model
     * @param m
     * @return
     */
    public static ArrayList<String> getImport(Model m){
        String queryImport =
                        "PREFIX owl: <http://www.w3.org/2002/07/owl#> " +
                        "SELECT ?importURI " +
                        "WHERE { ?o owl:imports ?importURI. }";

        return listURI(queryImport, m);
    }

    /**
     * returns a list of extended ontologies URIs in model
     * @param m
     * @return
     */
    public static ArrayList<String> getExtend(Model m){
        String queryExtend =
                        "PREFIX voaf: <http://purl.org/vocommons/voaf#> " +
                        "SELECT ?extendURI " +
                        "WHERE { ?o voaf:extends ?extendURI. }";

        return listURI(queryExtend, m);
    }

}
