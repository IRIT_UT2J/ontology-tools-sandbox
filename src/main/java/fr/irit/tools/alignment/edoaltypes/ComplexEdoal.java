package fr.irit.tools.alignment.edoaltypes;

public class ComplexEdoal {
    private String cell, class1, class2, attribute2, result2, relation;
    private double measure;

    public ComplexEdoal(String cell, String class1, String class2, String attribute2, String result2, String relation, double measure) {
        this.cell = cell;
        this.class1 = class1;
        this.class2 = class2;
        this.attribute2 = attribute2;
        this.result2 = result2;
        this.relation = relation;
        this.measure = measure;
    }

    public ComplexEdoal(String cell, String class1, String class2, String attribute2, String result2, String relation) {
        this.cell = cell;
        this.class1 = class1;
        this.class2 = class2;
        this.attribute2 = attribute2;
        this.result2 = result2;
        this.relation = relation;
        this.measure = 1.0f;
    }

    public String WriteComplex(){
        return ("<map>\n" +
                "\t<Cell rdf:about=\"" + cell + "\">\n" +
                "\t\t<entity1>\n" +
                "\t\t\t<edoal:Class rdf:about=\"" + class1 + "\"/>\n" +
                "\t\t</entity1>\n" +
                "\t\t<entity2>\n" +
                "\t\t\t<edoal:Class>\n" +
                "\t\t\t\t<edoal:and rdf:parseType=\"Collection\">\n" +
                "\t\t\t\t\t<edoal:Class rdf:about=\"" + class2 + "\"/>\n" +
                "\t\t\t\t\t\t<edoal:AttributeDomainRestriction>\n" +
                "\t\t\t\t\t\t\t<edoal:onAttribute>\n" +
                "\t\t\t\t\t\t\t\t<edoal:Relation rdf:about=\"" + attribute2 + "\"/>\n" +
                "\t\t\t\t\t\t\t</edoal:onAttribute>\n" +
                "\t\t\t\t\t\t\t<edoal:class>\n" +
                "\t\t\t\t\t\t\t\t<edoal:Class rdf:about=\"" + result2 + "\"/>\n" +
                "\t\t\t\t\t\t\t</edoal:class>\n" +
                "\t\t\t\t\t\t</edoal:AttributeDomainRestriction>\n" +
                "\t\t\t\t</edoal:and>\n" +
                "\t\t\t</edoal:Class>\n" +
                "\t\t</entity2>\n" +
                "\t\t<measure rdf:datatype=\"&xsd;float\">" + measure + "</measure>\n" +
                "\t\t<relation>" + relation + "</relation>\n" +
                "\t</Cell>\n" +
                "</map>");
    }

    public String WriteComplex(int order){
        if (order == 12) {
            return ("<map>\n" +
                    "\t<Cell rdf:about=\"" + cell + "\">\n" +
                    "\t\t<entity1>\n" +
                    "\t\t\t<edoal:Class rdf:about=\"" + class1 + "\"/>\n" +
                    "\t\t</entity1>\n" +
                    "\t\t<entity2>\n" +
                    "\t\t\t<edoal:Class>\n" +
                    "\t\t\t\t<edoal:and rdf:parseType=\"Collection\">\n" +
                    "\t\t\t\t\t<edoal:Class rdf:about=\"" + class2 + "\"/>\n" +
                    "\t\t\t\t\t\t<edoal:AttributeDomainRestriction>\n" +
                    "\t\t\t\t\t\t\t<edoal:onAttribute>\n" +
                    "\t\t\t\t\t\t\t\t<edoal:Relation rdf:about=\"" + attribute2 + "\"/>\n" +
                    "\t\t\t\t\t\t\t</edoal:onAttribute>\n" +
                    "\t\t\t\t\t\t\t<edoal:class>\n" +
                    "\t\t\t\t\t\t\t\t<edoal:Class rdf:about=\"" + result2 + "\"/>\n" +
                    "\t\t\t\t\t\t\t</edoal:class>\n" +
                    "\t\t\t\t\t\t</edoal:AttributeDomainRestriction>\n" +
                    "\t\t\t\t</edoal:and>\n" +
                    "\t\t\t</edoal:Class>\n" +
                    "\t\t</entity2>\n" +
                    "\t\t<measure rdf:datatype=\"&xsd;float\">" + measure + "</measure>\n" +
                    "\t\t<relation>" + relation + "</relation>\n" +
                    "\t</Cell>\n" +
                    "</map>");
        }
        else if (order == 21){
            return ("<map>\n" +
                    "\t<Cell rdf:about=\"" + cell + "\">\n" +
                    "\t\t<entity1>\n" +
                    "\t\t\t<edoal:Class>\n" +
                    "\t\t\t\t<edoal:and rdf:parseType=\"Collection\">\n" +
                    "\t\t\t\t\t<edoal:Class rdf:about=\"" + class2 + "\"/>\n" +
                    "\t\t\t\t\t\t<edoal:AttributeDomainRestriction>\n" +
                    "\t\t\t\t\t\t\t<edoal:onAttribute>\n" +
                    "\t\t\t\t\t\t\t\t<edoal:Relation rdf:about=\"" + attribute2 + "\"/>\n" +
                    "\t\t\t\t\t\t\t</edoal:onAttribute>\n" +
                    "\t\t\t\t\t\t\t<edoal:class>\n" +
                    "\t\t\t\t\t\t\t\t<edoal:Class rdf:about=\"" + result2 + "\"/>\n" +
                    "\t\t\t\t\t\t\t</edoal:class>\n" +
                    "\t\t\t\t\t\t</edoal:AttributeDomainRestriction>\n" +
                    "\t\t\t\t</edoal:and>\n" +
                    "\t\t\t</edoal:Class>\n" +
                    "\t\t</entity1>\n" +
                    "\t\t<entity2>\n" +
                    "\t\t\t<edoal:Class rdf:about=\"" + class1 + "\"/>\n" +
                    "\t\t</entity2>\n" +
                    "\t\t<measure rdf:datatype=\"&xsd;float\">" + measure + "</measure>\n" +
                    "\t\t<relation>" + relation + "</relation>\n" +
                    "\t</Cell>\n" +
                    "</map>");
        }
        else {
            return "";
        }
    }

    public String toString(){
        return ("cell = " + cell + "\nclass 1 = " + class1 + "\nclass 2 = " + class2 + "\nattribute = " + attribute2
                + "\nrestriction = " + result2 + "\nrelation = \"" + relation + "\"\nmeasure = " + measure);
    }
}
