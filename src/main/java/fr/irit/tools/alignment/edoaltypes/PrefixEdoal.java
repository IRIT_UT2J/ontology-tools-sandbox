package fr.irit.tools.alignment.edoaltypes;

public class PrefixEdoal {
    private String uri;
    private String prefix;

    public PrefixEdoal(String uri, String prefix) {
        this.uri = uri;
        this.prefix = prefix;
    }

    public String WritePrefix(){
        return ("<!ENTITY " + prefix + " \"" + uri + "\">");
    }
}
