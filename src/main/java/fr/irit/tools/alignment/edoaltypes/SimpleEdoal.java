package fr.irit.tools.alignment.edoaltypes;

public class SimpleEdoal {

    private String cell, ent1, ent2, relation, type;
    private double measure;

    private void setType(int numType){
        switch (numType){
            case 1:
                type = "Class";
                break;
            case 2:
                type = "Relation";
                break;
            case 3:
                type = "Property";
                break;
            default:
                System.out.println("No type available for this simple edoal");
                break;
        }
    }

    public SimpleEdoal(String cell, String ent1, String ent2, String relation, double measure, int numtype) {
        this.cell = cell;
        this.ent1 = ent1;
        this.ent2 = ent2;
        this.relation = relation;
        this.measure = measure;
        setType(numtype);
    }

    public SimpleEdoal(String cell, String ent1, String ent2, String relation, int numtype) {
        this.cell = cell;
        this.ent1 = ent1;
        this.ent2 = ent2;
        this.relation = relation;
        this.measure = 1.0f;
        setType(numtype);
    }

    public String WriteSimple(){
        return ("<map>\n" +
                "\t<Cell rdf:about=\"" + cell + "\">\n" +
                "\t\t<entity1>\n" +
                "\t\t\t<edoal:" + type + " rdf:about=\"" + ent1 + "\"/>\n" +
                "\t\t</entity1>\n" +
                "\t\t<entity2>\n" +
                "\t\t\t<edoal:" + type + " rdf:about=\"" + ent2 + "\"/>\n" +
                "\t\t</entity2>\n" +
                "\t\t<measure rdf:datatype=\"&xsd;float\">" + measure + "</measure>\n" +
                "\t\t<relation>" + relation + "</relation>\n" +
                "\t</Cell>\n" +
                "</map>");
    }

    public String toString(){
        return ("uri 1 = " + ent1 + "\nuri 2 = " + ent2 + "\nrelation = \"" + relation + "\"\nmeasure = " + measure);
    }
}
