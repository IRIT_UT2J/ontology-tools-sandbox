package fr.irit.tools.alignment.main;

import fr.irit.tools.alignment.GUI.GUIFactory;
import fr.irit.tools.alignment.ontology.OntIndexer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Controller {
    private String uri1, uri2;
    private OntIndexer ont1;
    private OntIndexer ont2;
    private PrintWriter out;
    private HashMap<String, String> prefix;

    /** unique instance pre-initialized (singleton) */
    private static Controller INSTANCE = new Controller();

    public static Controller getInstance() {
        return INSTANCE;
    }

    /** launches the application */
    private void launch(){
        prefix = new HashMap<>();
        GUIFactory.createGui(GUIFactory.TypeWindows.LAUNCH);
    }

    /** shuts down the application
     * (after closing the printwriter on the file if opened) */
    public void Exit(){
        if (out != null) {
            out.close();
        }
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        System.exit(0);
    }

    public String getUri1() {
        return uri1;
    }

    public void setUri1(String uri1) {
        this.uri1 = uri1;
    }

    public String getUri2() {
        return uri2;
    }

    public void setUri2(String uri2) {
        this.uri2 = uri2;
    }

    public boolean isURI(String string){
        return (string.startsWith("http://") || string.startsWith("https://"));
    }

    public void getMenu(String uri1, String uri2){
        ont1 = new OntIndexer(uri1);
        ont2 = new OntIndexer(uri2);
        GUIFactory.createGui(GUIFactory.TypeWindows.MENU);
    }

    public void getTypeSet(){
        GUIFactory.createGui(GUIFactory.TypeWindows.TYPESET);
    }

    public void getPrefix(){ GUIFactory.createGui(GUIFactory.TypeWindows.PREFIX, out); }

    public void getSimple(int type){ GUIFactory.createGui(GUIFactory.TypeWindows.SIMPLE, out, type); }

    public void getStructSet(){
        GUIFactory.createGui(GUIFactory.TypeWindows.STRUCTSET);
    }

    public void getComplex(int type){
        GUIFactory.createGui(GUIFactory.TypeWindows.COMPLEX, out, type);
    }

    /**
     * fonction de test à enlever
     * @param s
     */
    public void containsbaduri(String s){
        if (s.contains("http://iserve.kmi.open.ac.uk/ns/msm/msm-2014-09-03.rdf") ||
                s.contains("http://purl.oclc.org/NET/muo/muo-vocab.owl") ||
                s.contains("http://www.wsmo.org/ns/wsmo-lite") ||
                s.contains("http://vocab.org/lifecycle/schema-20080603.rdf") ||
                s.contains("http://www.ontologydesignpatterns.org/schemas/cpannotationschema.owl")){
            System.out.println("TROUVE");
        }

        if(s.contains("http://elite.polito.it/ontologies/dogont.owl"))
        {
            System.out.println("WINTER HAS COME");
        }
    }

    /**
     * returns a list of URIs of input type on specified ont (URI)
     * @param ont
     * @param type
     * @return
     */
    public ArrayList<String> GenerateList(int ont, int type){
        ArrayList<String> box = new ArrayList<>();
        ArrayList<String> l;
        Set<String> set;

        if (ont == 1) {
            set = ont1.getSet(type);
            for (String entity : set)
            {
                l = ont1.getListURI(entity, type);
                for (String uri : l)
                {
                    box.add(entity + " (" + uri + ")");
                    //containsbaduri(uri);
                }
            }
        }
        else if (ont == 2){
            set = ont2.getSet(type);
            for (String entity : set)
            {
                l = ont2.getListURI(entity, type);
                for (String uri : l)
                {
                    box.add(entity + " (" + uri + ")");
                    //containsbaduri(uri);
                }
            }
        }
        return box;
    }

    /** returns the list of all imported URIs correctly formated (ending with '#' or '/')*/
    public ArrayList<String> getAllURI(){
        ArrayList<String> uriList = new ArrayList<>();

        for (String uri : ont1.getListImport())
        {
            if ((uri.endsWith("#") || uri.endsWith("/")) && !uriList.contains(uri)){
                uriList.add(uri);
            }
        }

        for (String uri : ont2.getListImport())
        {
            if ((uri.endsWith("#") || uri.endsWith("/")) && !uriList.contains(uri)){
                uriList.add(uri);
            }
        }
        return uriList;
    }

    /** saves the duo (uri,prefix) in hashmap */
    public void setPrefixForOnt(String uri, String pfx){
        prefix.put(uri, ("&" + pfx + ";") );
    }
    /** returns prefix set for specified URI */
    public String getPrefixForOnt(String uri){
        return prefix.get(uri);
    }

    /**
     * returns string prefix + entity if prefix is set for fixed URI
     * else returns unchanged URI
     * @param fulluri
     * @return
     */
    public String CheckPrefixForOnt(String fulluri){
        if (prefix != null) {
            String entity;
            String uri;
            int pos;

            if (fulluri.contains("#")){
                pos = fulluri.indexOf("#");
                entity = fulluri.substring(pos+1, fulluri.length());
                uri = fulluri.substring(0, pos+1);
            }
            else {
                pos = fulluri.lastIndexOf("/");
                entity = fulluri.substring(pos+1, fulluri.length());
                uri = fulluri.substring(0, pos+1);
            }

            if (prefix.containsKey(uri)) {
                return (prefix.get(uri) + entity);
            }
            else {
                return fulluri;
            }
        }
        else {
            return fulluri;
        }
    }


    /**
     * opens printwriter on input file
     * @param file
     */
    public void ChooseFile(String file){
        try {
            out = new PrintWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args){
        Controller controller = Controller.getInstance();
        controller.launch();
    }
}
