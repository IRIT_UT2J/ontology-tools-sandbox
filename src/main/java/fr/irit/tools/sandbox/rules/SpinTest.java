package fr.irit.tools.sandbox.rules;

import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileUtils;
import org.apache.jena.vocabulary.RDF;
//import org.spinrdf.arq.ARQ2SPIN;
//import org.spinrdf.arq.ARQFactory;
//import org.spinrdf.model.Select;
//import org.spinrdf.system.SPINModuleRegistry;

public class SpinTest {
public static void main(String[] args) {
		
		// Register system functions (such as sp:gt (>))
//		SPINModuleRegistry.get().init();
		
		// Create an empty OntModel importing SP
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("rdf", RDF.getURI());
		model.setNsPrefix("ex", "http://example.org/demo#");
		
		String query =
			"SELECT ?person\n" +
			"WHERE {\n" +
			"    ?person a ex:Person .\n" +
			"    ?person ex:age ?age .\n" +
			"    FILTER (?age > 18) .\n" +
			"}";
		
//		Query arqQuery = ARQFactory.get().createQuery(model, query);
//		ARQ2SPIN arq2SPIN = new ARQ2SPIN(model);
//		Select spinQuery = (Select) arq2SPIN.createQuery(arqQuery, null);
		
//		System.out.println("SPIN query in Turtle:");
//		model.write(System.out, FileUtils.langTurtle);
//		
//		System.out.println("-----");
//		String str = spinQuery.toString();
//		System.out.println("SPIN query:\n" + str);
//		
//		// Now turn it back into a Jena Query
//		Query parsedBack = ARQFactory.get().createQuery(spinQuery);
//		System.out.println("Jena query:\n" + parsedBack);
	}
}
