package fr.irit.tools.sandbox.ontology;

import java.io.File;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

public class OWLapi {
	public static void shouldLoad() throws Exception {
		// Get hold of an ontology manager
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		
//		IRI remoteOntology = IRI.create("http://xmlns.com/foaf/spec/");
		IRI remoteOntology = IRI.create("https://www.irit.fr/recherches/MELODI/ontologies/IoT-O");
//		IRI remoteOntology = IRI.create("http://purl.oclc.org/NET/ssnx/ssn");
		// Load the ontology as if we were loading it from the web (from its
		// ontology IRI)
		OWLOntology redirectedOntology = manager.loadOntology(remoteOntology);
		System.out.println(redirectedOntology);
	}
	
	public static void shouldLoadLocal() throws Exception {
        // Get hold of an ontology manager
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File("/home/nseydoux/Bureau/IoT-O.owl"));
//        OWLOntology ontology = load(manager);
        // We can always obtain the location where an ontology was loaded from;
        // for this test, though, since the ontology was loaded from a string,
        // this does not return a file
//        IRI documentIRI = manager.getOntologyDocumentIRI(ontology);
//        // In cases where a local copy of one of more ontologies is used, an
//        // ontology IRI mapper can be used to provide a redirection mechanism.
//        // This means that ontologies can be loaded as if they were located on
//        // the web. In this example, we simply redirect the loading from
//        // an IRI to our local copy above.
//        // iri and file here are used as examples
//        IRI iri = ontology.getOntologyID().getOntologyIRI().get();
//        IRI remoteOntology = IRI.create("http://remote.ontology/we/dont/want/to/load");
//        manager.getIRIMappers().add(new SimpleIRIMapper(remoteOntology, iri));
//        // Load the ontology as if we were loading it from the web (from its
//        // ontology IRI)
//        OWLOntology redirectedOntology = manager.loadOntology(remoteOntology);
        // Note that when imports are loaded an ontology manager will be
        // searched for mappings
        System.out.println(ontology);
}
	
	public static void main(String[] args) {
		try {
			shouldLoad();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
