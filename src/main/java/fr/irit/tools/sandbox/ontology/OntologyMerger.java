package fr.irit.tools.sandbox.ontology;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.jena.rdf.model.Model;

import fr.irit.tools.utils.ModelLoader;
import fr.irit.tools.utils.ontologies.DUL;
import fr.irit.tools.utils.ontologies.Dogont;
import fr.irit.tools.utils.ontologies.IoTO;
import fr.irit.tools.utils.ontologies.OPA;
import fr.irit.tools.utils.ontologies.SAN;
import fr.irit.tools.utils.ontologies.SSN;

/**
 * 
 * Outputs a model (mergedModel.rdf) containing other models. Useful for SWIP
 */
public class OntologyMerger {
	public static void main(String[] args) {
		Model m = ModelLoader.getLoadedModel(IoTO.getInstance(), /*OPA.getInstance(),*/ Dogont.getInstance(), SAN.getInstance(), SSN.getInstance(), DUL.getInstance());
		try {
			m.write(new FileWriter(new File("model.owl")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
