package fr.irit.tools.sandbox.ontology;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

public class OntologyConverter {
	public static void main(String[] args) {
		Model m = ModelFactory.createDefaultModel();
		m.read("data/opa-appartment.ttl", "TTL");
		try {
			m.write(new FileWriter(new File("opa-appartment.owl")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
