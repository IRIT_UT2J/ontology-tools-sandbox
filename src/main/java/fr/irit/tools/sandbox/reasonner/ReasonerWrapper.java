package fr.irit.tools.sandbox.reasonner;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;


public class ReasonerWrapper {
	public static Model reason(ReasonerType reasonerType, Model kb){
    	Reasoner reasoner;
    	switch(reasonerType){
    	case RDFS:
    		reasoner = ReasonerRegistry.getRDFSReasoner();
    		break;
    	case OWLMicro:
    		reasoner = ReasonerRegistry.getOWLMicroReasoner();
    		break;
    	case OWLMini:
    		reasoner = ReasonerRegistry.getOWLMiniReasoner();
    		break;
    	case OWL:
    		reasoner = ReasonerRegistry.getOWLReasoner();
    		break;
		default:
			reasoner = ReasonerRegistry.getOWLReasoner();
    	}
    	return ModelFactory.createInfModel(reasoner, kb);
    }
}
