package fr.irit.tools.sandbox.reasonner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.util.FileManager;

public class Saturator {
	public static void main(String[] args) {

		// Reasoner reasoner = PelletReasonerFactory.theInstance().create();
		Reasoner reasoner = ReasonerRegistry.getRDFSReasoner();
		Model init = FileManager.get().loadModel("file:ontologies/OPA.owl");
		System.out.println("C'est tipar, gros");
		// create an inferencing model using Pellet reasoner
		InfModel model = ModelFactory.createInfModel(reasoner, init);
		System.out.println("Raisonnement OK");
		// Model m = ModelFactory.createDefaultModel();
		// m.add(model);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File("inferredOPA.owl")));
			model.write(bw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
