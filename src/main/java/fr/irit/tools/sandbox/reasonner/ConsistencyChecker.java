package fr.irit.tools.sandbox.reasonner;

import java.util.Iterator;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.util.FileManager;

import openllet.jena.PelletReasonerFactory;



public class ConsistencyChecker {
	
	public static void main(String[] args) {
		
		Reasoner reasoner = PelletReasonerFactory.theInstance().create();
		Model base = ModelFactory.createDefaultModel();
		
		// Loading of the different ontologies/kb at stake
		Model opa_data = FileManager.get().loadModel("file:ontologies/OPA_data.owl");
		base.add(opa_data);
		Model opa_schema = FileManager.get().loadModel("file:ontologies/OPA_schema.owl");
		base.add(opa_schema);
		Model ioto = FileManager.get().loadModel("file:ontologies/IoT-O.owl");
		base.add(ioto);
		Model ssn = FileManager.get().loadModel("file:ontologies/SSN_custom.owl");
		base.add(ssn);
		Model san = FileManager.get().loadModel("file:ontologies/SAN.owl");
		base.add(san);
		Model states = FileManager.get().loadModel("file:ontologies/ObjectWithStates.owl");
		base.add(states);
		Model iot_lifecycle = FileManager.get().loadModel("file:ontologies/IoT-Lifecycle.owl");
		base.add(iot_lifecycle);
		Model lifecycle = FileManager.get().loadModel("file:ontologies/lifecycle.rdf");
		base.add(lifecycle);
		Model dogont = FileManager.get().loadModel("file:ontologies/dogont.owl");
		base.add(dogont);
		Model muo = FileManager.get().loadModel("file:ontologies/muo.rdf");
		base.add(muo);
//		Model dul = FileManager.get().loadModel("file:ontologies/DUL.owl");
//		opa.add(dul);
		
		// create an inferencing model using Pellet reasoner
		final InfModel model = ModelFactory.createInfModel(reasoner, base);
		ValidityReport validity = model.validate();
		
		// Display of the validity report
		if (validity.isValid()) {
		    System.out.println("OK");
		} else {
		    System.out.println("Conflicts");
		    for (Iterator i = validity.getReports(); i.hasNext(); ) {
		        System.out.println(" - " + i.next());
		    }
		}
	}
	
	
}
