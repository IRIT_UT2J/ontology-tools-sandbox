package fr.irit.tools.sandbox.reasonner;

public enum ReasonerType {
	NONE("none"), RDFS("rdfs"), OWLMicro("owl-micro"), OWLMini("owl-mini"), OWL("owl");
	
	private String type;
	private ReasonerType(String type){
		this.type = type;
	}
	
	/**
	 * @param type
	 * @return the reasoner type with the label corresponding to the parameter string, null if it is unknown
	 */
	public static ReasonerType getReasonerByType(String type){
		for(ReasonerType r : ReasonerType.values()){
			if(r.type.equals(type.toLowerCase())){
				return r;
			}
		}
		return null;
	}
	
	public String toString(){
		return this.type;
	}
}
