package fr.irit.tools.queries;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;

import fr.irit.tools.utils.ModelLoader;
import fr.irit.tools.utils.ontologies.Ontology;

/**
 * This class allows simple and ad-hoc inference based on SPARQL queries
 */
public class QueryReasoner {
	
	/**
	 * 
	 * @param kb The KB supporting the inference
	 * @param query a SPARQL CONSTRUCT query
	 * @param requiredOntologies list of ontologies and KB required by the process (imports and such)
	 * @return The constructed inferences
	 */
	public static Model queryInference(Model kb, String query, Ontology... requiredOntologies) {
		Model tmp_m = ModelLoader.getLoadedModel(requiredOntologies);
		tmp_m.add(kb);
		QueryExecution qexec = QueryExecutionFactory.create(
				query, tmp_m);
		return qexec.execConstruct();
	}
}
