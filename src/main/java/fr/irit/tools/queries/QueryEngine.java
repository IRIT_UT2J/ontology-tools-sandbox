package fr.irit.tools.queries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.update.UpdateAction;

public class QueryEngine {
	
	public static List<Map<String, String>> selectQuery(String queryString, Model m) {
		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, m)) {
			ResultSet results = qexec.execSelect();
			while (results.hasNext()) {
				QuerySolution soln = results.next();
				resultList.add(new HashMap<String, String>());
				List<String> varNameTestList = new ArrayList<String>();
				Iterator<String> querySolutionVariables = soln.varNames();
				while (querySolutionVariables.hasNext()) {
					varNameTestList.add(querySolutionVariables.next());
				}
				// On va ajouter pour chaque résultat une map contenant les noms
				// de variables et leurs valeurs
				for (String varName : varNameTestList) {
					RDFNode x = soln.get(varName); // Get a result variable by
													// name.
					resultList.get(resultList.size() - 1).put(varName, x.toString());
				}
			}
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Model describeQuery(String queryString, Model m) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, m)) {
			Model result = qexec.execDescribe();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Boolean askQuery(String queryString, Model m) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, m)) {
			return qexec.execAsk();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Model constructQuery(String queryString, Model m) {
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, m)) {
			Model result = qexec.execConstruct();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void updateQuery(String queryString, Model m) {
		UpdateAction.parseExecute(queryString, m);
	}
}
