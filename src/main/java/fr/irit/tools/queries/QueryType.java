package fr.irit.tools.queries;

public enum QueryType {
	SELECT, UPDATE, ASK, DESCRIBE;
}
