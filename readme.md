Ontology manipulation toolbox
=============================

Tools suite for ontology manipulation and validation

# Available tools

## Consistency checker

The code has to be modified to include the ontologies you want to validate. The code then runs a fork of the pellet reasoner to verify the absence of inconsistencies.

## Ontologies

The philosophy of this package is to group constants regarding ontologies, and to manipulate them easily. To do so, each class gives the URI, the preferred prefix, and some elements of an ontology. These classes are reused for different purposes in the other packages.

# Getting started

Clone the git repository, and compile and run with maven. The code requires java 8 to run. Before compiling, you need to install spinrdf : `git clone https://github.com/spinrdf/spinrdf.git`, then `mvn install`

- __Compiling__: `mvn compile`
- __Running__: `mvn exec:java`

By default, the executed main is the consistency checker. To change that, modify the pom.xml (until the interface is cleaner).

